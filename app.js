var express = require('express');
var app = express();

app.use('/MockResponse',  express.static(__dirname + '/MockResponse'));


app.get('/',function(req,res){
	res.sendFile('home.html',{'root': __dirname + '/MockResponse'});
})

app.get('/speaker',function(req,res){
	res.sendFile('speakers.json',{'root': __dirname + '/MockResponse'});
})

app.get('/news',function(req,res){
	res.sendFile('news.json',{'root': __dirname + '/MockResponse'});
})

app.get('/schedule',function(req,res){
	res.sendFile('schedule.json',{'root': __dirname + '/MockResponse'});
})

app.listen(3000,function(){
    console.log('Node server running @ http://localhost:3000')
});
